package com.backend.capstone.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;
import java.util.Set;

import static javax.persistence.CascadeType.PERSIST;

@Entity
@Table(name="bookings")
public class Booking {
    public Booking(){}
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @OneToMany(fetch = FetchType.LAZY,mappedBy ="booking",cascade = PERSIST)
    @JsonIgnore
    Set<Attendee> attendees;

    @Column
    String description;

    @Column
    LocalDateTime bookDate;

    @Column
    LocalDate startDate;

    @Column
    LocalDate endDate;

    @Column
    LocalTime startTime;

    @Column
    LocalTime endTime;

    @Column
    String purpose;

    @Column
    String referenceNo;

    @Column
    int computers;
    @Column
    String status;

    @Column
    float durationHours;

    @Column
    String user;

    @Column
    float coins;

    @Column
    float points;

    @Column
    int participants;

    @ManyToOne
    @JoinColumn(name="venue_id")
    Venue venue;


    public Set<Attendee> getAttendees() {
        return attendees;
    }

    public void setAttendees(Set<Attendee> attendees) {
        this.attendees = attendees;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public LocalDateTime getBookDate() {
        return bookDate;
    }

    public void setBookDate(LocalDateTime bookDate) {
        this.bookDate = bookDate;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public void setStartDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }

    public void setEndDate(LocalDate endDate) {
        this.endDate = endDate;
    }

    public LocalTime getStartTime() {
        return startTime;
    }

    public void setStartTime(LocalTime startTime) {
        this.startTime = startTime;
    }

    public LocalTime getEndTime() {
        return endTime;
    }

    public void setEndTime(LocalTime endTime) {
        this.endTime = endTime;
    }

    public String getPurpose() {
        return purpose;
    }

    public void setPurpose(String purpose) {
        this.purpose = purpose;
    }

    public String getReferenceNo() {
        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        this.referenceNo = referenceNo;
    }

    public int getComputers() {
        return computers;
    }

    public void setComputers(int computers) {
        this.computers = computers;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public float getDuration() {
        return durationHours;
    }

    public void setDuration(float durationHours) {
        this.durationHours = durationHours;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public float getCoins() {
        return coins;
    }

    public void setCoins(float coins) {
        this.coins = coins;
    }

    public float getPoints() {
        return points;
    }

    public void setPoints(float points) {
        this.points = points;
    }

    public Venue getVenue() {
        return venue;
    }

    public void setVenue(Venue venue) {
        this.venue = venue;
    }

    public float getDurationHours() {
        return durationHours;
    }

    public void setDurationHours(float durationHours) {
        this.durationHours = durationHours;
    }

    public int getParticipants() {
        return participants;
    }

    public void setParticipants(int participants) {
        this.participants = participants;
    }
    public void addAttendee(Attendee attendee) {
        attendee.setBooking(this);
        attendees.add(attendee);
    }


}
