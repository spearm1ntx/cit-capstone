package com.backend.capstone.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="users")
public class User {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String username;
    @Column
    //makes the password invisible during a get during serialization from java object to json format
    @JsonProperty(access= JsonProperty.Access.WRITE_ONLY)
    private String password;

    //@OneToMany(mappedBy="user")
    //prevent infinite recursion with bidirectional relationship
    //@JsonIgnore
    //collection of posts that are unique
    //private Set<Post> posts;


    public User(){

    }
    public User(String username,String password){
        this.username=username;
        this.password=password;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }



}
