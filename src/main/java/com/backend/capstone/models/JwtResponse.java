package com.backend.capstone.models;

import java.io.Serializable;

public class JwtResponse implements Serializable {
    private static final long serialVersionUID = -8962959817456490965L;

    private final String jwttoken;

    public JwtResponse(String jwttoken){this.jwttoken=jwttoken;}

    public String getToken(){return this.jwttoken;}
}
