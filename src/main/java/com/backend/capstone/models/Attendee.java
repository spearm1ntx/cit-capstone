package com.backend.capstone.models;

import javax.persistence.*;

@Entity
@Table(name="attendees")
public class Attendee {

    public Attendee() {
    }

    public Attendee(String user) {
        this.user = user;
    }
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column
    String user;
    @ManyToOne
    @JoinColumn(name="booking_id")
    Booking booking;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Booking getBooking() {
        return booking;
    }

    public void setBooking(Booking booking) {
        this.booking = booking;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }


}
