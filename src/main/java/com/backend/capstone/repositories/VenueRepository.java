package com.backend.capstone.repositories;

import com.backend.capstone.models.Venue;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VenueRepository extends CrudRepository<Venue,Object> {
}
