package com.backend.capstone.repositories;

import com.backend.capstone.models.Attendee;
import com.backend.capstone.models.Booking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AttendeeRepository extends CrudRepository<Attendee,Object> {

}
