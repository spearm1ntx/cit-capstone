package com.backend.capstone.repositories;

import com.backend.capstone.models.Booking;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BookingRepository extends CrudRepository<Booking,Object> {
    Booking findByReferenceNo(String refNo);
}
