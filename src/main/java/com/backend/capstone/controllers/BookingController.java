package com.backend.capstone.controllers;

import com.backend.capstone.models.Attendee;
import com.backend.capstone.models.Booking;
import com.backend.capstone.models.BookingRequest;
import com.backend.capstone.repositories.AttendeeRepository;
import com.backend.capstone.services.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.Set;

@RestController
@CrossOrigin
public class BookingController {
    @Autowired
    BookingService bookingService;
    @Autowired
    AttendeeRepository attendeeRepository;
    @GetMapping("/booking")
    public Iterable getBooking(){
        return bookingService.getBookings();
    }
    @PostMapping("/booking/{venueid}")
    public ResponseEntity createBooking(@RequestBody BookingRequest booking,@PathVariable(name="venueid") Long venueId,@RequestHeader(name="Authorization")String stringToken) throws ParseException {
        return bookingService.addBooking(booking,venueId,stringToken);
    }
    @GetMapping("/booking/{bookingid}/getAttendees")
    public Iterable<?> getAttendees(@PathVariable(name="bookingid")Long bookingid){
        return bookingService.getAttendee(bookingid);
    }
}
