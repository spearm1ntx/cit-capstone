package com.backend.capstone.services;

import com.backend.capstone.config.JwtToken;
import com.backend.capstone.models.Attendee;
import com.backend.capstone.models.Booking;
import com.backend.capstone.models.BookingRequest;
import com.backend.capstone.models.Venue;
import com.backend.capstone.repositories.AttendeeRepository;
import com.backend.capstone.repositories.BookingRepository;
import com.backend.capstone.repositories.VenueRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.util.Set;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Service
public class BookingServiceImpl implements BookingService {
    @Autowired
    BookingRepository bookingRepository;
    @Autowired
    VenueRepository venueRepository;
    @Autowired
    JwtToken jwtUtil;
    @Autowired
    AttendeeRepository attendeeRepository;


    @Override
    public Iterable<Booking> getBookings() {
        return bookingRepository.findAll();
    }

    @Override
    public Iterable<?> getAttendee(Long id) {
        Booking booking= bookingRepository.findById(id).get();
        return booking.getAttendees();
    }



    @Override
    public ResponseEntity addBooking(BookingRequest booking,Long venueId,String stringToken) throws ParseException {
        //get venue
        Venue bookingVenue= venueRepository.findById(venueId).get();
        String username=jwtUtil.getUsernameFromToken(stringToken);

        Booking bookingToSave=new Booking();
        bookingToSave.setBookDate(LocalDateTime.now());
        bookingToSave.setDescription(booking.getDescription());
        //format date string;
        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd");
        //convert date to Local date
        LocalDate startLocalDate= inputFormat.parse(booking.getStartDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endLocalDate= inputFormat.parse(booking.getEndDate()).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        bookingToSave.setStartDate(startLocalDate);
        bookingToSave.setEndDate(endLocalDate);
        //purpose
        bookingToSave.setPurpose(booking.getPurpose());

        //computers
        bookingToSave.setComputers(booking.getComputers());

        //point or coins
        bookingToSave.setPoints(booking.getPoints());
        bookingToSave.setCoins(booking.getCoins());

        //venue
        bookingToSave.setVenue(bookingVenue);

        //username
        bookingToSave.setUser(username);

        //status
        bookingToSave.setStatus("Booked");

        //time saving
        bookingToSave.setStartTime(LocalTime.parse(booking.getStartTime()));
        bookingToSave.setEndTime(LocalTime.parse(booking.getEndTime()));
        //calculation of duration and setting duration
        SimpleDateFormat format2= new SimpleDateFormat("yy-MM-dd HH:mm:ss");
        Date start=format2.parse(booking.getStartDate()+" "+booking.getStartTime());
        Date end=format2.parse(booking.getEndDate()+" "+booking.getEndTime());
        System.out.print(start+"----"+end);
        long duration= (end.getTime()-start.getTime());
        float floatValDuration=((float) TimeUnit.MILLISECONDS.toMinutes(duration))/60;
        bookingToSave.setDuration(floatValDuration);

        //String Reference no. by random generation
        UUID randomUUID=UUID.randomUUID();
        String res=randomUUID.toString().replaceAll("-", "").substring(0,6).toUpperCase();

        while(bookingRepository.findByReferenceNo(res)!=null) {
         randomUUID=UUID.randomUUID();
         res=randomUUID.toString().replaceAll("-", "").substring(0,6).toUpperCase();
        }
        bookingToSave.setReferenceNo(res);
        //participants
        bookingToSave.setParticipants(booking.getParticipants());
        //saving attendees


        //final saving
        bookingRepository.save(bookingToSave);
        Attendee owner=new Attendee(username);
        owner.setBooking(bookingToSave);
        attendeeRepository.save(owner);

        for(String user:booking.getAttendees()){
            Attendee attendee=new Attendee(user);
            attendee.setBooking(bookingToSave);
            attendeeRepository.save(attendee);
        }


        return new ResponseEntity("Booked Succesfully!", HttpStatus.OK);
    }
}
