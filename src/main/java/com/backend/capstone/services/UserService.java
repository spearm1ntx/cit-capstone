package com.backend.capstone.services;

import com.backend.capstone.models.User;
import org.springframework.http.ResponseEntity;

import java.util.Optional;

public interface UserService {
    ResponseEntity createUser(User user);
    Iterable<User> getUsers();
    ResponseEntity deleteUser(Long id);

    ResponseEntity updateUser(Long id,User user);



    Optional<User> findByUsername(String username);


}
