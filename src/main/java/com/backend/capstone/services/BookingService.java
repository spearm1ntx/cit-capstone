package com.backend.capstone.services;

import com.backend.capstone.models.Attendee;
import com.backend.capstone.models.Booking;
import com.backend.capstone.models.BookingRequest;
import org.springframework.http.ResponseEntity;

import java.text.ParseException;
import java.util.Set;

public interface BookingService {
    Iterable<Booking> getBookings();

    Iterable<?> getAttendee(Long id);
    ResponseEntity addBooking(BookingRequest booking,Long venueId,String stringToken) throws ParseException;
}
