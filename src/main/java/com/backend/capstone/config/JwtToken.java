package com.backend.capstone.config;

import com.backend.capstone.models.User;
import com.backend.capstone.repositories.UserRepository;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtToken implements Serializable {
    @Value("${jwt.secret}")
    private String secret;

    @Autowired
    private UserRepository userRepository;

    private static final long serialVersionUID= 8698350881324240943L;

    //Time duration in seconds that the token can be used.
    public static final long JWT_TOKEN_VALIDITY=5*60*60;

    public JwtToken(){}
    private String doGenerateToken(Map<String, Object> claims, String subject){
        return Jwts.builder()
                .setClaims(claims)
                .setId(claims.get("user").toString())
                .setSubject(subject)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis()+(JWT_TOKEN_VALIDITY*1000)))
                .signWith(SignatureAlgorithm.HS512,secret)
                .compact();
    }
    public String generateToken(UserDetails userDetails){
        Map<String, Object> claims = new HashMap<>();
        User user = userRepository.findByUsername(userDetails.getUsername());

        //id is added to the claims of JWT to be used to indetify the user in the server
        //claims is the data within the jwt
        claims.put("user",user.getId());

        return doGenerateToken(claims, userDetails.getUsername());
    }

    //token Validation by extracting the username from the token
    public Boolean validateToken(String token,UserDetails userDetails){
        //Extract username from token and store it in a variable
        final String username = getUsernameFromToken(token);
        //returns boolean that indicates if token is valid or not
        //checks if username from user details matches the username from the token
        //checks if token is expired or not
        return (username.equals(userDetails.getUsername())&&!isTokenExpired(token));
    }



    //extract the subject username from jwt toekn
    public String getUsernameFromToken(String token) {
        String claim= getClaimFromToken(token, Claims::getSubject);
        return claim;
    }

    public <T> T getClaimFromToken(String token, Function<Claims,T> claimsResolver){

        final Claims claims=getAllClaimsFromToken(token);
        //extracts the specific claim value.
        return claimsResolver.apply(claims);
    }

    //extracts all the claims from the token.
    private Claims getAllClaimsFromToken(String token){
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();

    }

    //used to extract expiration date of token.
    public Date getExpirationDateFromToken(String token){
        return getClaimFromToken(token, Claims::getExpiration);
    }
    //check if the jwt token has expired
    private Boolean isTokenExpired(String token){
        final Date expiration=getExpirationDateFromToken(token);
        //true if current date is after expiration
        //false if current date is before expiration
        return expiration.before(new Date());
    }

    public Long getIdFomToken(String stringToken) {


        return Long.parseLong(getClaimFromToken(stringToken, Claims::getId));
    }
}
