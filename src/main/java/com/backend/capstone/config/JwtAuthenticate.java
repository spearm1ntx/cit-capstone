package com.backend.capstone.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;


public class JwtAuthenticate implements AuthenticationEntryPoint, Serializable {

    // serialVersionUID serves as the "state" of a serializable object. This is used by Java in deserializing a
    // serialized object.
    //serialization is the process of transmitting information in a different data structure (ie. an object is
    // serialized into a string to be transmitted and gets deserialized back into an object when it reaches its
    // destination
    private static final long serialVersionUID = -7858869558953243875L;

    @Override
    // This will handle the authentication failures.
    public void commence(HttpServletRequest request, HttpServletResponse response,
                         // AuthenticationException is a class in Spring Security that contains all exceptions related to an Authentication object being invalid
                         AuthenticationException authException) throws IOException,ServletException {
        // This will send a HTTP error response with a 401 status code with an "Unauthorized" message to the client.
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");

    }


}
